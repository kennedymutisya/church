<?php

namespace App;

use App\Reports\Reports;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function reports($collection)
    {
        return new Reports($collection);
    }
}
