<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Resources\PledgeResource;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\UserResource;
use App\Member;
use App\Pledge;
use App\Project;
use App\User;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    protected $group;

    /**
     * GroupController constructor.
     * @param $group
     */
    public function __construct(Group $group)
    {
        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = $this->group::all();
        return view('groups.view', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = $this->group::all();
        return view('groups.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required']
        ]);
        $this->group->name = $request->name;
        $this->group->saveOrFail();

        return $this->group;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $members = $group->members()->get();
        return view('groups.show', compact('group', 'members'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //
    }

    public function addMember(Request $request, Group $group, Member $member)
    {
        $this->validate($request, [
            'member_id' => ['required'],
            'group_id' => ['required'],
        ]);
        $group = $group::find($request->group_id);
        $group->members()->attach($request->member_id);
        return $member::find($request->member_id);

    }

    public function deleteMember(Request $request, Group $group)
    {
        $group = $group::find($request->group_id);
        $group->members()->detach($request->member_id);

        return "Completed";
    }

    public function resource()
    {
        return new ProjectResource(Project::find(1));
    }
}
