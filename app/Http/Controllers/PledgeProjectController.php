<?php

namespace App\Http\Controllers;

use App\PledgeProject;
use Illuminate\Http\Request;

class PledgeProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request,PledgeProject $project)
    {

        $project->project_id = $request->project;
        $project->member_id = $request->id;
        $project->amount = $request->amount;
        $project->saveOrFail();

        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PledgeProject  $pledgeProject
     * @return \Illuminate\Http\Response
     */
    public function show(PledgeProject $pledgeProject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PledgeProject  $pledgeProject
     * @return \Illuminate\Http\Response
     */
    public function edit(PledgeProject $pledgeProject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PledgeProject  $pledgeProject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PledgeProject $pledgeProject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PledgeProject  $pledgeProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(PledgeProject $pledgeProject)
    {
        //
    }
}
