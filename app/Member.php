<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Member
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $occupation
 * @property string $mobileNumber
 * @property string $yearofbirth
 * @property string $email
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereOccupation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereYearofbirth($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tithe[] $tithe
 */
class Member extends Model
{

    public function tithe()
    {
        return $this->hasMany(Tithe::class, 'member_id', 'id');
    }

    public function pledge()
    {
        return $this->belongsTo(Pledge::class, 'member_id', 'id');
    }

    public function projects()
    {
        return $this->hasMany(Pledge::class, 'member_id', 'id');
    }
}
