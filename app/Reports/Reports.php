<?php
/**
 * Created by PhpStorm.
 * User: Kennedy Mutisya
 * Date: 1/5/2018
 * Time: 11:04 AM
 */

namespace App\Reports;


class Reports
{
    protected $reports;

    public function __construct($reports)
    {
        $this->reports = $reports;
    }

    public function allMembers()
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.allmembers',['reports'=>$this->reports]);
        return $pdf->stream();
    }
}