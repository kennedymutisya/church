<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tithe
 *
 * @property int $id
 * @property string $date
 * @property string $memberName
 * @property int $tithe
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereMemberName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereTithe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $member_id
 * @property-read \App\Tithe $member
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tithe whereMemberId($value)
 */
class Tithe extends Model
{
    public function member()
    {
        return $this->hasOne(Tithe::class, 'id', 'member_id');
    }
}
