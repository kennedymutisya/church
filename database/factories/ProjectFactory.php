<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'projectName' => $faker->company,
        'startDate' => $faker->dateTimeThisMonth,
        'endDate' => $faker->dateTimeThisMonth,
        'budget' => $faker->randomNumber(),
    ];
});
