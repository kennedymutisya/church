<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name('M'),
        'address' => $faker->address,
        'occupation' => $faker->jobTitle,
        'mobileNumber' => $faker->e164PhoneNumber,
        'yearofbirth' => $faker->year,
        'email' => $faker->safeEmail,
    ];
});
