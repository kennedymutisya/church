<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Guards::class, function (Faker $faker)
{
    return [
        'name' => $faker->name('M'),
        'phonenumber' => $faker->phoneNumber,
        'idnumber' => $faker->numberBetween('30014060', '31015656'),
        'email' => $faker->email,
        'employedDate' => $faker->dateTimeThisYear,
    ];
});

$factory->state(\App\Guards::class, 'assigned', function ($faker)
{
    return [
        'client_id' => factory(\App\Client::class)->create()->id,

    ];
});
