@extends('layouts.bravo')
@section('title')
    Showing Project
@stop
<?php /** @var \App\Project $project */ ?>
@section('content')
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"><img width="100%" alt="user" src="{{ asset('plugins/images/big/project.jpg') }}">
                </div>
                <div class="user-btm-box">
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Project Name</strong>
                            <p>{{ $project->projectName }}</p>
                        </div>
                        <div class="col-md-6"><strong>End Date</strong>
                            <p>{{ $project->endDate }}</p>
                            `
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Budget</strong>
                            <p>{{ number_format($project->budget,2) }}</p>
                        </div>
                        <div class="col-md-6"><strong>Start Date</strong>
                            <p>{{ $project->startDate }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-12"><strong>Contributions</strong>
                            1000
                        </div>
                    </div>
                    <hr>
                    <!-- /.row -->
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12 ">
                        <a href="{{ route("projectreportmember",[$contributions[0]['id'],$project]) }}" class="btn btn-sm btn-primary pull-right">
                            <i class="glyphicon glyphicon-envelope"></i>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <addpledge :members="{{ \App\Member::all() }}" :projectid="{{$project->id}}"></addpledge>
                    </div>
                </div>
                <div class="row">
                    <allpledges :contribution="{{ json_encode($contributions) }}"
                                :projectid="{{$project->id}}"></allpledges>
                </div>
            </div>
        </div>
    </div>
@stop