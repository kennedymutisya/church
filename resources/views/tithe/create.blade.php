@extends('layouts.bravo')
@section('title')
    Tithe
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <tithe :members="{{ $members }}"></tithe>
                    </div>
                    <div class="panel-footer"></div>
                </div>
            </div>
            <hr>
        </div>
    </div>
@stop