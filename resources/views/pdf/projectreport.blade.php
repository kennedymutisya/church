<html>
<head>
    <title>Project Report</title>
    <script language="JavaScript" type="text/javascript">
        setTimeout("window.print();", 10000);
    </script>
    <style>
        body {
            padding: 10px;
            margin: 10px;
            font-size: 9px;
        }

        table {
            font-family: Verdana;
            font-size: 12px;
            empty-cells: show;
            border: 1px solid #000;
            border-collapse: collapse;
            border-spacing: 0.5rem;
            empty-cells: show;
        }

        td {
            border: 1px solid #000;
        }

        td.abottom {
            vertical-align: bottom;
            font-size: 10px;
        }

        td.bold {
            font-weight: bold;
        }

        span.title {
            font-size: 14px;
            font-weight: bold;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 0px;
                padding: 0px;
            }
        }

        @media screen {
            .page-break {
                display: block;
                page-break-before: always;
                margin: 5px;
                padding: 5px;
            }
        }

    </style>

</head>
<body>

<table width="100%" style="border:0;" cellspacing="0" cellpadding="3" class="data">
    <tr>
        <td colspan="15">

            <table width="100%" style="border:0;" cellspacing="0" cellpadding="1" class="data">

                <tr>
                    <td rowspan="6" style="border:0;" width="25%">
{{--                        <img style="border:0;" src="../../public/images/school_logo.jpg">--}}
                    </td>
                </tr>

                <tr>
                    <td valign="top" colspan="3" style="border:0;"><span
                                class="title">Makindu Redeemed Gospel Church</span></td>
                </tr>

                <tr>
                    <td colspan="3" style="border:0;"><b>Address : P.O BOX 28 - 90138 MAKINDU</b></td>
                </tr>

                <tr>
                    <td colspan="3" style="border:0;"><b>Tel :0713642175</b></td>
                </tr>


                <tr>
                    <td colspan="3" style="border:0;">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="2" style="border:0;"></td>
                    <td colspan="2" style="border:0;" align="right"><b>Date : </b> {{ \Carbon\Carbon::today()->toFormattedDateString() }}</td>
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan="15">
            <h4>PROJECT : {{mb_strtoupper($project->projectName)}} <br>
                PROJECT BUDGET : {{number_format($project->budget)}} <br>


            </h4>
        </td>
    </tr>
    <tr>
        <td>#</td>
        <td width="100%">Name</td>
        <td width="100%">Pledge</td>
        <td colspan="12">Balance</td>
    </tr>
    @foreach($contributions as $cont)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ mb_strtoupper($cont->member->name) }}</td>
            <td>{{ number_format($cont->amount,2) }}</td>
            <td colspan="12">{{ number_format(($cont->amount - $cont->payments->sum('amount')),2) }}</td>
        </tr>

    @endforeach

</table>
</body>
</html>

